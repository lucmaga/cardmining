from datastruct.deck import CardList
from datastruct.card import *

card = Card()
card.name = u'Angry Chicken'
cardList = CardList()
cardList.addCard(card)
print cardList.getCardDictionary()
print cardList.hasCard(card)
cardList.addCard(card)
print cardList.hasCard(card)
print cardList.getCardDictionary()
print cardList.getCards()
print cardList.numberOfCards()
print cardList.removeCard(card)

card2 = Card
card2.name = u'Kel\'Thuzad'
print cardList.hasCard(card2)
cardList.addCard(card2)
print cardList.getCardDictionary()
print cardList.hasCard(card2)
cardList.addCard(card2)
print cardList.hasCard(card2)
print cardList.getCardDictionary()
print cardList.getCards()
print cardList.numberOfCards()
print cardList.removeCard(card2)
print cardList.getCardDictionary()

