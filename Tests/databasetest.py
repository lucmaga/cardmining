from datastruct.card import *
from database.database import *

#Create Angry Chicken
card = Card()
card.name = u'Angry Chicken'
setattr(card, '_set', 'Basic')
setattr(card, 'power', 1)
setattr(card, 'life', 1)

# Create Kel'Thuzad
card2 = Card()
card2.name = u'Kel\'Thuzad'
setattr(card2, '_set', 'Naxxaramas')
setattr(card2, 'power', 6)
setattr(card2, 'life', 8)

# Create Grim Patron
card3 = Card()
card3.name = 'Grim Patron'
setattr(card3, '_set', 'Blackrock Mountain')
setattr(card3, 'power', 3)
setattr(card3, 'life', 3)

# Create Holly Nova
card4 = Card()
card4.name = u'Holly Nova'
setattr(card4, '_set', 'Naxxaramas')
setattr(card4, 'text', 'Deal 2 damage to all enemies. Restore #2 Health to all friendly characters.')

# Add and get a card
cardDb = Database(2,)
cardDb.insert(card, [card.name, card._set])
print card.name + ' Insert on database.'
print cardDb.get(card.name)
print '[%s]' % ', '.join(map(str, cardDb.get(card._set,1)))

# Add the second card and try get both
cardDb.insert(card2, [card2.name, card2._set])
print '\n\n'
print card2.name + ' Insert on database.'
print cardDb.get(card.name)
print '[%s]' % ', '.join(map(str, cardDb.get(card._set,1)))
print cardDb.get(card2.name)
print '[%s]' % ', '.join(map(str, cardDb.get(card2._set,1)))

# Add the third card and get all
cardDb.insert(card3, [card3.name, card3._set])
print '\n\n'
print card3.name + ' Insert on database.'
print cardDb.get(card.name)
print '[%s]' % ', '.join(map(str, cardDb.get(card._set,1)))
print cardDb.get(card2.name)
print '[%s]' % ', '.join(map(str, cardDb.get(card2._set,1)))
print cardDb.get(card3.name)
print '[%s]' % ', '.join(map(str, cardDb.get(card3._set,1)))

# Add the forth card and get all
cardDb.insert(card4, [card4.name, card4._set])
print '\n\n'
print card4.name + ' Insert on database.'
print cardDb.get(card.name)
print '[%s]' % ', '.join(map(str, cardDb.get(card._set,1)))
print cardDb.get(card2.name)
print '[%s]' % ', '.join(map(str, cardDb.get(card2._set,1)))
print cardDb.get(card3.name)
print '[%s]' % ', '.join(map(str, cardDb.get(card3._set,1)))
print cardDb.get(card4.name)
print '[%s]' % ', '.join(map(str, cardDb.get(card4._set,1)))


a = cardDb.search(lambda x: x._set == 'Basic')
print '[%s]' % ', '.join(map(str, a))
a = cardDb.search(lambda x: hasattr(x,'power') and x.power > 2)
print '[%s]' % ', '.join(map(str, a))

print cardDb.remove('Grim Patron')
print cardDb.get('Grim Patron')
