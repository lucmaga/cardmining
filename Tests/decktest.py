from datastruct.deck import Deck
from datastruct.card import *

card = Card()
card.name = u'Angry Chicken'
card2 = Card
card2.name = u'Kel\'Thuzad'

deck = Deck()
deck.addCard(card)
deck.addCard(card)
deck.addCard(card)
deck.addCard(card2)
deck.addCard(card2)
deck.addCard(card2)
deck.removeCard(card2)
deck.removeCard(card2)
deck.removeCard(card2)
deck.removeCard(card)
deck.removeCard(card)
deck.removeCard(card)

