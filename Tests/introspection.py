import types


class Person:
    def __init__(self, name):
        self.name = name

    @classmethod
    def add_attribute(cls, attr):
        attr = '_' + attr

        def get_attribute(self):
            return getattr(self, attr, None)

        def set_attribute(self, value):
            return setattr(self, attr, value)

        setattr(cls, 'get_' + attr, types.MethodType(get_attribute, cls))
        setattr(cls, 'set_' + attr, types.MethodType(set_attribute, cls))


me = Person('Hossomi')
print me.name
print dir(me)

Person.add_attribute('age')
print dir(me)
print me.get_age()

me.set_age(22)
print dir(me)
print me.get_age()

