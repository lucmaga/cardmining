from util.messages import *

class DatabaseError(Exception):

    def __init__(self,value):
        if type(value) is not str:
            raise TypeError("value must be str type.")
        self.value = value

    def __str__(self):
        return self.value



class Element:
    """
    This is a simple genetic element holder for the database.
    """
    def __init__(self, element, listOfKeys):
        self.element = element
        self.listOfKeys = listOfKeys
        pass

    def __str__(self):
        return str(self.element)


class Database:
    """
    Some Important things about the Database.
    The element list is a list of dictionaries. Each dictionary references one of the keys.
    The primary key must be unique for element. The other keys can repeat.
    The Structure of each dictionary is like {'<key>': [<element>]}
    For the primary key the structure is {'<key>': <element>}
    """

    """
    The database constructor.
    This wil initialize the elements list and will configure the number of keys used in the database.
    """
    def __init__(self, numberOfKeys=1):
        self._elements = []
        self._numberOfKeys = numberOfKeys
        for i in range(numberOfKeys):
            self._elements.append({})

        pass

    def insert(self, element, listOfKeys):
        if self._numberOfKeys != len(listOfKeys):
            raise DatabaseError("Wrong number of keys: expect {0}, has {1}".format(self.numberOfKeys, len(listOfKeys)))

        try:
            if listOfKeys[0] in self._elements[0].keys():
                raise DatabaseError('Duplicated primary key: ' + str(listOfKeys[0]))
            elem = Element(element, listOfKeys)
            self._elements[0][listOfKeys[0]] = elem
        except IndexError as e:
            Message.error_print("PrimaryKey: {2}, IndexError {0}: {1}".format(e.errno, e.strerror,str(listOfKeys[0])))
            return -1
#        except TypeError as e:
#            Message.error_print("PrimaryKey: {1}, TypeError: {0}".format(str(e), str(listOfKeys[0])))
#            return -1

        i = 1
        for key in listOfKeys[1:]:
            try:
                self._elements[i][key].append(elem)
            except KeyError:
                self._elements[i][key] = [elem]
            i += 1
        pass

    """
    The remove just remove a element by his primary key.
    This way the user must know better what he is removing.
    """
    def remove(self, primaryKey):
        element = self._elements[0][primaryKey]
        for d,k in zip(self._elements, element.listOfKeys):
            del d[k]

    """
    Search is a interface where is possible to apply a search in the database using a lambda.
    The lambda will be used as a filter on the list of elements.
    Be sure that a search will never be as fast as a get. So never uses search if you can use a get.
    the lambda will be applyed not in the elements but in the elements it holds. this way it is easy
    fo the user.
    """
    def search(self, logic=None):
        l = list(self._elements[0].values())
        l = map(lambda e: e.element,l)
        return filter(logic, l)

    def get(self, key, typeOfKey=0):
        if self._numberOfKeys <= typeOfKey:
            raise DatabaseError("Wrong number of key: expect {0} > {1}".format(self._numberOfKeys, typeOfKey))
        try:
            if type(self._elements[typeOfKey][key]) is list:
                return map(lambda x: x.element, self._elements[typeOfKey][key])
        except KeyError:
            return -1
        except TypeError as e:
            Message.error_print(str(e) + ' Type is ' + str(type(self._elements[typeOfKey][key])))

        return self._elements[typeOfKey][key].element
