# Card Class Definition

from util import introspection


class Card:
    def __init__(self, name=''):
        self.name = name
        pass

    def print_pretty(self):
        d = self.__dic__()
        for attr in d.keys():
            print '\n'.join([
                '%s %s %s' % (
                    attr.ljust(20),
                    str(type(d[attr])).ljust(20),
                    d[attr]
                )
            ])

    def __str__(self):
        return self.name

    def __dic__(self):
        d = {}

        # Use introspection to obtain all attributes
        attrlist = introspection.get_attributes(self)
        for attr in attrlist:
            d[attr] = getattr(self, attr, None)

        return d

    def __eq__(self, b):
        return self.name == b.name
