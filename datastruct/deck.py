#Deck Class Definition

from util import introspection
from util.messages import Message

class CardList:
    """This dictionary has a cardName as key and associate it with a list
    with a card and the number of times it appears"""
    cardDictionary = {}

    def __init__(self):
        pass

    def addCardsByList(self, listOfCards, listOfNumbers):
        for card,number in listOfCards, listOfCards:
            self.cardDictionary.append({card.name:[card ,number]})

    def setCardDictionary(self, cardList):
        if cardList is CardList:
            self.cardDictionary = cards.getCardDictionary()

    def __eq__(self,b):
        for card in self.cardDictionary:
            if not b.hasCards(card[0]) == card[1]:
                return False
        return True

    def getCards(self):
        return [self.cardDictionary[card][0] for card in self.cardDictionary]

    def hasCard(self, card):
        if self.cardDictionary.has_key(card.name):
            return self.cardDictionary[card.name][1]
        return None

    def getCardDictionary(self):
        return self.cardDictionary

    def numberOfCards(self):
        sum = 0
        for card in self.cardDictionary:
            sum += self.cardDictionary[card][1]
        return sum

    def addCard(self, card):
        if self.cardDictionary.has_key(card.name):
            self.cardDictionary[card.name][1] += 1
        else:
            self.cardDictionary[card.name] = [card, 1]

    def removeCard(self, card, number = 1):
        if self.hasCard(card) is None:
            return False
        elif self.hasCard(card) <= number:
            self.cardDictionary.pop(card.name)
            return True
        else:
            self.cardDictionary[card.name][1] -= number
            return True


class Deck:

    arena = ''
    author = ''
    cardList = None
    deckClass = None
    name = ''
    rating = ''
    deckType = ''

    def __init__(self, arena='', author='', cardList= None, deckClass='', includeUnreleasedCards='', includeReleasedCards='', name='', rating='', deckType=''):
        self.arena = arena
        self.author = author
        self.cardList = CardList()
        self.cardList.cardDictionary = cardList  if cardList is not None else {}
        self.deckClass = deckClass
        self.includeUnreleasedCards = includeUnreleasedCards
        self.includeReleasedCards = includeReleasedCards
        self.name = name
        self.rating = rating
        self.deckType = deckType

    def __eq__(self, b):
        return self.cardList == b.getCardList()

    """We need a way to return if the card is not added"""
    def addCard(self, card, number = 1):
        if self.cardList.numberOfCards() < 30:
            if self.cardList.hasCard(card) is None or self.cardList.hasCard(card) + number <= 2:
                for i in range(number):
                    self.cardList.addCard(card)
            else:
                text = 'There is already ' + str(self.cardList.hasCard(card)) +  ' cards named ' + card.name + ' in this Deck. Cannot add ' + number + 'cards'
                Message.error_print(text)
        else:
            Message.error_print('This Deck already has 30 cards in it.')
            print self.cardList.cardDictionary
            print self


    def removeCard(self, card):
        if self.cardList.hasCard(card) > 0:
            self.cardList.removeCard(card)
        else:
            text = 'There is no card named ' + card.name + ' in this Deck.'
            print text


    def __dic__(self):
        d = {}

        # Use introspection to obtain all attributes
        attrlist = introspection.get_attributes(self)
        for attr in attrlist:
            if attr == 'cardList' :
                d[attr] = self.cardList.getCardDictionary()
            else:
                d[attr] = getattr(self, attr, None)

        return d

    def print_pretty(self):
        d = self.__dic__()
        for attr in d.keys():
            if attr == 'cardList':
                print '\n'.join([
                    '%s %s [%s]' % (
                        attr.ljust(20),
                        str(type(d[attr])).ljust(20),
                        '%s' % ', '.join(map(str, self.cardList.getCardDictionary().keys()))
                        )
                    ])
            else:
                print '\n'.join([
                '%s %s %s' % (
                    attr.ljust(20),
                    str(type(d[attr])).ljust(20),
                    d[attr]
                )
            ])
