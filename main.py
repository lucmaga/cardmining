import sys

from parser import cardparser
from util.messages import Message
from util import introspection
from database.database import Database, DatabaseError
from parser.htmldeckparser import HTMLDeckParser
from parser.deckparser import *
import re

def main(argv):
    print "Hossomi says \"hello world!\""
    print "Magal says \"Greetings!\""
    print "Magal says \"I will hunt you down!\""
    print "Rodrigo Says \"Your soul shall be mine!\""

    if len(argv) < 2:
        Message.error_print('For now I must know which file to parse. '
                            'Suggestion: look in \'resources\' folder.')
        sys.exit(1)

    cardlist = cardparser.parse_file(argv[1])
    print 'Parsed', len(cardlist), 'cards!'

    print '[%s]' % ', '.join(map(str, cardlist))

    cardlist[len(cardlist)-1].print_pretty()
    a = cardlist[len(cardlist)-1]
    print a.playerClass

    cardDb = Database(2)
    reg = re.compile(ur'^[A-Z]{2,3}[0-9]{0,1}_[0-9]{3}$')
    for card in cardlist:
#        if 'o' in card.id or 'e' in card.id or 'c' in card.id or 'h' in card.id or 't' in card.id or 'a' in card.id or 'f' in card.id: continue
        if not re.match(reg, card.id) or 'XXX' in card.id: continue
        try:
            try:
                cardDb.insert(card, [card.name, card.playerClass])
            except AttributeError as a:
                setattr(card,'playerClass','Neutral')
                cardDb.insert(card, [card.name, card.playerClass])
        except DatabaseError as e:
            Message.error_print(str(e))
    print cardDb.get('Arcane Explosion').print_pretty()
    deckParser = HTMLDeckParser(cardDb)
    firstDeck = deckParser.parseURL('http://www.hearthpwn.com/decks/270598-new-era-dragon-sage-legend')

    if type(firstDeck) is int:
        Message.error_print('Deck return ' + str(firstDeck))
        sys.exit()
    firstDeck.print_pretty()

    decktojson([firstDeck], 'tentativa1.json')


if __name__ == '__main__':
    main(sys.argv)
