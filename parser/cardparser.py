"""
Contains functions for parsing card data from a json file.
"""

import json
import types
from datastruct.card import Card
from util.messages import Message
from util import unicodeutil
from util import introspection


def parse(data):
    """
    Parse a card from data in dictionary form
    """

    card = Card()
    for key in data.keys():
        value = data[key]

        # try to convert unicode to string
        if isinstance(value, types.UnicodeType):
            value = unicodeutil.unicode_to_string(value)

        setattr(card, key, value)

    return card


def parse_set(setdata):
    """
    Parse all cards from a list of data in dictionary form
    """

    cardlist = []
    for cardData in setdata:
        cardlist.append(parse(cardData))

    return cardlist


def parse_file(jsonfilename):
    """
    Parse all cards from a json file depending on the structure found:
    - List: assumes it is a set (list of cards)
    - Dictionary: assumes it is a list of sets
    """

    try:
        jsonfile = open(jsonfilename)
        data = json.load(jsonfile)
        jsonfile.close()

        # if a list: read single set
        if type(data) is types.ListType:
            cardlist = parse_set(data)

        # if a dict: read and unite several sets
        elif type(data) is types.DictionaryType:
            cardlist = []
            for setname in data.keys():
                cardlist = cardlist + parse_set(data[setname])

    except ValueError:
        Message.error_print('It is definitely not friday 13... I mean, file is '
                            'not JSON!')
        raise

    except:
        Message.error_print('This JSON contains strange content!')
        raise

    return cardlist
