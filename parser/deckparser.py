import json
import types
from datastruct.deck import *
from util.messages import Message
from util import unicodeutil
from util import introspection


def parser(data):

    deck = Deck()
    for key in data.key():
        value = data[key]


        # try to convert unicode to string
        if isinstance(value, types.UnicodeType):
            value = unicodeutil.unicode_to_string(value)

        setattr(deck, key, value)

    return deck


def parse_file(jsonfilename):
    """
    Parse all cards from a json file depending on the structure found:
    - List: assumes it is a set (list of cards)
    - Dictionary: assumes it is a list of sets
    """

    try:
        jsonfile = open(jsonfilename)
        data = json.load(jsonfile)
        jsonfile.close()

        decklist = []
        for deck in data:
            decklist.append(parse_set(data))
    except ValueError:
        Message.error_print('It is definitely not friday 13... I mean, file is not JSON!')
        raise

    except:
        Message.error_print('This JSON contains strange content!')
        raise

    return cardlist


def decktojson(listOfDecks, filename):
    data = '{"decks":[\n'
    for deck in listOfDecks:
        data += '{\n'
        dic = deck.__dic__()
        for i,key in enumerate(dic):
            if type(dic[key]) is str or type(dic[key]) is unicode:
                data += '"'+key+'":"'+dic[key]+'"'
            elif type(dic[key]) is int:
                data += '"'+key+'":"'+str(dic[key])+'"'
            elif key == 'cardList':
                data += '"list":['
                for i, card in enumerate(dic[key].keys()):
                    data += '{"card":"'+card+'", "number":'+str(dic[key][card][1])+'}'
                    if i - len(dic) != 0:
                        data += ','
                data += ']'
            if i - len(dic) != 0:
                data += ','
        data += '}]}'
    f = open(filename, 'w')
    f.write(data)
    f.close()
    return

