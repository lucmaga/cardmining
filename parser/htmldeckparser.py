# -*- coding: utf-8 -*-
import urllib
import re
from util.messages import Message
from datastruct.card import Card
from datastruct.deck import *

debug = True

class HearthpwnParser(object):
    """This Class Parse a particular string """
    reg = ur'<b style.+><a.+>(?P<name>.+)</a></b>\r\n\r\nX (?P<number>\d)'

#   @staticmethod
    def parse(self, htmlString, cardDatabase):
        reg = re.compile(self.reg)
        htmlString = unicode(htmlString, "utf-8")
        htmlString = htmlString.replace(u'×', 'X')
        htmlString = htmlString.replace(u'&#x27;', "'")
        #print htmlString
        iterator = re.findall(reg,htmlString)
        if iterator == []:
            return None
        self.deck = Deck()
        print self.deck
        for cardN in iterator:
            card = cardDatabase.get(cardN[0])
            if type(card) is int and card == -1:
                Message.error_print("Card "+cardN[0]+" not found in Database")
                return None
            Message.debug_print("Adding Card "+str(card)+" "+str(int(cardN[1]))+" times.")
            self.deck.addCard(card,int(cardN[1]))
        global debug
        if debug:
            Message.debug_print('Deck extracted:' + str(self.deck))
        reg = re.compile(ur'<li class=\"name\"><a href=\".+\" class=\"tip\" title=\"(?P<author>.+)\">')
        self.deck.author = re.search(reg,htmlString).group(1)
        reg = re.compile(ur'<h2 class=\"deck-title tip\" title=\"(?P<name>.+)\">')
        self.deck.name = re.search(reg,htmlString).group(1)
        reg = re.compile(ur'<li>Deck Type: <span class=\"deck-type\">(?P<type>.+)</span></li>')
        self.deck.deckType = re.search(reg,htmlString).group(1)
        reg = re.compile(ur'<div class="rating-sum rating-average rating-average-ratingPositive tip" title="Click to see breakdown">(?P<rating>.+)</div>')
        self.deck.rating = int(re.search(reg,htmlString).group(1))
        return self.deck

class HTMLDeckParser(object):
    """docstring for HTMLDeckParser"""
    parsers = {'hearthpwn': HearthpwnParser}

    def __init__(self, cardDatabase):
        self._cardDatabase = cardDatabase

    """ Returns a tuple with a deck from a url"""
    def parseURL(self, url):
        for i in self.parsers.keys():
            if re.search(i, url) != None :
                htmlString = urllib.urlopen(url).read()
                #print htmlString
                parser = self.parsers[i]()
                return parser.parse(htmlString, self._cardDatabase)
        global debug
        if debug:
            Message.error_print('Deck can\'t be loaded. Ther is no parser for:' +  url)
        return None

    def _nameToDeck(self, listOfNames):
        deck = Deck()
        for name in listOfNames:
            for i in range(int(name['number'])):
                Message.debug_print('Inserting: ' + name['name'])
                card = self._cardDatabase.get(name['name'])
                if type(card) is int and card == -1:
                    Message.error_print('Fail to get ' + name['name'])
                    return -1
                deck.addCard(card)
        return deck


