import sys
import json

if len(sys.argv) < 2:
    print 'Usage: formatter.py <input json>'
    exit()

name = sys.argv[1]

inFile = open(name)
data = json.load(inFile)
print json.dumps(data, indent=4)
