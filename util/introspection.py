
def get_attributes(object):
    return [attr for attr in dir(object) if not callable(getattr(object, attr)) and attr[0:2] != '__']
