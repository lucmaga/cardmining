
class Message():
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

    @staticmethod
    def debug_print(string):
        print Message.OKGREEN + 'Debug: ' + Message.ENDC + string


    @staticmethod
    def error_print(string):
        print Message.FAIL + 'Error: ' + Message.ENDC + string


    @staticmethod
    def warning_print(string):
        print Message.WARNING + 'Warning: ' + Message.ENDC + string
