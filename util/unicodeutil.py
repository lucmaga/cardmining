from messages import Message
import pickle

# some constants
_REPLACEMENTS_FILE = 'util/unicode_replacements'

# unicode string replacements
global _replacements
_replacements = {}

# set to True if you want the user to be asked for a replacement for each
# problematic unicodecharacter.
global _askforreplacement
_askforreplacement = False


def unicode_to_string(value):
    done = False
    while not done:
        done = True
        try:
            value = str(value)

        except UnicodeEncodeError as e:
            done = False
            u = value[e.start]
            if u in _replacements:
                Message.warning_print('Replacing \'%s\' with \'%s\''
                                      % (u, _replacements[u]))
                value = value.replace(u, _replacements[u])
            elif _askforreplacement:
                unicode_ask_replacement(u)
                value = value.replace(u, _replacements[u])
            else:
                Message.warning_print('Replacing \'%s\' with \'?\''
                                      % u)
                value = value.replace(u, '?')

    return value


def unicode_ask_replacement(value):
    Message.warning_print('Cannot encode unicode \'' + value + '\'')
    replace = raw_input('Replacement: ')
    _replacements[value] = replace
    unicode_save_replacements()


def unicode_save_replacements():
    outfile = open(_REPLACEMENTS_FILE, 'w')
    pickle.dump(_replacements, outfile)
    outfile.close()


def unicode_load_replacements():
    try:
        infile = open(_REPLACEMENTS_FILE    )
        global _replacements
        _replacements = pickle.load(infile)
        infile.close()

    except IOError:
        print 'Could not load unicode replacements file, will ask for them!'
        global _askforreplacement
        _askforreplacement = True
